// CKEDITOR.plugins.add( 'talkingmarkers', {
//     icons: 'talkingmarkers',
//     init: function( editor ) {
//         //Plugin logic goes here.
//     }
// });

/**
 * Copyright (c) 2014-2018, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/ckeditor4/docs/#!/guide/plugin_sdk_intro
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'talkingmarkers', {

	// Register the icons. They must match command names.
	icons: 'talkingmarkers',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define the editor command that inserts a timestamp.
		editor.addCommand( 'talkingmarkers', {

			// Define the function that will be fired when the command is executed.
			exec: function( editor ) {
        
        // TODO show modal with marker list options
				var now = new Date();
				// Insert the timestamp into the document.
				//editor.insertHtml( 'The current date and time is: <em>' + now.toString() + '</em>' );
        alert('talkingmarkers actions function ...');
			}
		});

		// Create the toolbar button that executes the above command.
		editor.ui.addButton( 'Timestamp', {
			label: 'Insert Marker Action',
			command: 'talkingmarkers',
			toolbar: 'others',
      icon: this.path +'icons/talkingmarkers.png'
		});
	}
});