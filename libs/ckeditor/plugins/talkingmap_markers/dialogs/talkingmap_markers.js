CKEDITOR.dialog.add( 'talkingmap_markersDialog', function( editor ) {
    return {
        title: 'Markers Action',
        minWidth: 400,
        minHeight: 200,
      
        onShow: function(){
          $('#markerActionButton').html('<code>No marker selected.</code>');
        },

        contents: [
            {
                id: 'tab-zoomto',
                label: 'Zoom To',
                elements: [
                    // UI elements of the first tab    will be defined here.
                  {
                      type: 'text',
                      id: 'markerActionButtonLabel',
                      className:'markerActionButtonLabel',
                      label: 'Action Button Text',
                      style: '',
                      'default': 'ZoomTo',
                      validate: function() {
                          if ( !this.getValue() ) {
                              api.openMsgDialog( '', 'ZoomLevel cannot be empty.' );
                              return false;
                          }
                        // check for numeric input or replace this with a select with only numbers for zoom levels
                      },
                      onChange: function(api){
                         //change action button text dinamically
                         $('#markerActionButton .marker-zoom-action').html(this.getValue());
                      }
                  },
                  {
                      type: 'text',
                      id: 'zoomLevel',
                      className:'zoomLevelMarkerAction',
                      label: 'ZoomLevel 1-20',
                      style: 'width:20%!important;',
                      'default': '13',
                      validate: function() {
                          if ( !this.getValue() ) {
                              api.openMsgDialog( '', 'ZoomLevel cannot be empty.' );
                              return false;
                          }
                        // check for numeric input or replace this with a select with only numbers for zoom levels
                      },
                       onChange: function(api){
                         //TODO change dinamically the zoom of the main map accordingly to this value
                         
                       }
                  },
                                    
                  {
                      type: 'select',
                      id: 'dialod_markers',
                      label: 'Select a Marker to Zoom To',
                      items: [[]],//populate_markers(),
                      //'default': '- Please Select one Marker -',
//                       onLoad: function( api ){
//                         // Populate the select with all markers from this slide if there is a map
//                         alert('Populate the select with all markers from this slide if there is a map...');
//                       },
                      onShow: function(element){ 
                        var items = this;
                        items.clear();                 
                        //items.add('- Please Select one Marker -');
                        var markers = populate_markers(); 
                        markers.forEach( function(e){items.add(e[0],e[1])} ); 
                      },
                      onChange: function( api ) {
                          // this = CKEDITOR.ui.dialog.select
                          //alert( 'Current value: ' + this.getValue() );
                          //console.log(JSON.parse(this.getValue().split('>> ')[1]));
                          var marker = JSON.parse(this.getValue());
                          var zooml = parseInt($('.zoomLevelMarkerAction input').val());
                          var label = $('.markerActionButtonLabel input').val();
                          GlobalContents.maps[ get_current_mainslide_id() ].map.zoomToMarker(marker.lat,marker.lon,zooml);
                          $('#markerActionButton').html('<button class="btn btn-dark marker-zoom-action"\
onclick=\'GlobalContents.maps[\"'+get_current_mainslide_id()+'"]\
.map.zoomToMarker('+marker.lat+','+marker.lon+','+zooml+')\'>'+label+'</button>');
                      }
                  },
                  {
                    type: 'select',
                    id: 'select_button_class',
                    label: 'Action Button Type',
                    items: [['Dark','btn-dark'],['Light','btn-light'],['Link','btn-link'],
                            ['Info','btn-info'],['Success','btn-success'],['Warning','btn-warning'],['Danger','btn-danger'],
                            ['Primary','btn-primary'],['Secondary','btn-secondary'],],
                    'default': 'btn-dark',
                    onChange: function(api){
                      //alert(this.getValue());
                      $('#markerActionButton .marker-zoom-action').removeClass('btn-dark btn-light btn-link btn-info btn-success btn-warning btn-danger btn-primary btn-secondary');
                      $('#markerActionButton .marker-zoom-action').addClass(this.getValue());
                    }
                  },
                  {
                      type: 'checkbox',
                      id: 'openpopup',
                      label: 'Open Popup on zoom',
                      'default': 'checked',
                      onChange:function(){
                        
                      },
                      onClick: function() {
                          // this = CKEDITOR.ui.dialog.checkbox
                          //alert( 'Checked: ' + this.getValue() );
                      }
                  },
                  {
                      type: 'html',
                      html: '<hr></hr>Your Action Button, press OK to insert it:'
                  },
                  {
                      type: 'html',
                      html: '<div id="markerActionButton"></div>'
                  }
                ]
            },
            {
                id: 'tab-more',
                label: 'Coming Soon...',
                elements: [
                    // UI elements of the second tab will be defined here.
                  {
                      type: 'button',
                      id: 'buttonId',
                      label: 'Coming Soon...',
                      title: 'Coming Soon...',
                      onClick: function() {
                          // this = CKEDITOR.ui.dialog.button
                          alert( 'More Actions Coming Soon...' );
                      }
                  },
                  {
                      type: 'html',
                      html: '<div id="markerAnimatetable">...</div>'
                  }
                ]
            }
        ],
      onOk: function(){
            var dialog = this;
            $('#markerActionButton code').remove();
            var prev_btn = '<span>' + $('#markerActionButton').html() + '</span>';
            console.log(prev_btn);        
            editor.insertHtml(prev_btn);        
      }
    };
});

function populate_markers(){
//  var markers = [ [ 'Duccio' ], [ 'Baseball' ], [ 'Hockey' ], [ 'Football' ] ];
  var markers = [ ['- Please Select one Marker -'] ];
  if (typeof GlobalContents.maps[ get_current_mainslide_id() ] != "undefined") {
//    Object.keys(GlobalContents.maps[ get_current_mainslide_id() ].markers).forEach(function(key){
//      var obj = GlobalContents.maps[ get_current_mainslide_id() ].markers[key];
//      markers.push( [ key + ' - ' + obj.title  , '{"lat":'+obj.lat + ',"lon":' + obj.lon + '}'] );
    Object.keys(GlobalContents.maps[ get_current_mainslide_id() ].map.markers_f._layers).forEach(function(key){
      var obj = GlobalContents.maps[ get_current_mainslide_id() ].map.markers_f._layers[key];
      console.log(obj);
      var title = "todo";//get title form ...
      markers.push( [ key + ' - ' + title  , '{"lat":'+obj._latlng.lat + ',"lon":' + obj._latlng.lng + ',"mid":'+obj._leaflet_id+'}'] );
    })
  };
  return markers;
}

// function zoomToMarker(divid,marker_id,lat,lon,zoom,popupopen){
//   GlobalContents.maps[ divid ].map.zoomToMarker(lat,lon,zoom);
//   if (popupopen){
//     GlobalContents.maps[divid].map.markers_f._layers[marker_id].openPopup();    
//   }
// }

// function populate_marker_table(){
  
// }