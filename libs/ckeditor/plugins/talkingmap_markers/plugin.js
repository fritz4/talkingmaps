// CKEDITOR.plugins.add( 'talkingmarkers', {
//     icons: 'talkingmarkers',
//     init: function( editor ) {
//         //Plugin logic goes here.
//     }
// });

/**
 * Copyright (c) 2014-2018, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/ckeditor4/docs/#!/guide/plugin_sdk_intro
 * example from
 * https://ckeditor.com/docs/ckeditor4/latest/guide/plugin_sdk_sample_1.html#dialog-window-tabs-elements
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'talkingmap_markers', {

	// Register the icons. They must match command names.
	icons: 'talkingmap_markers',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

		// Define the editor command that inserts a timestamp.
		editor.addCommand( 'talkingmap_markers', new CKEDITOR.dialogCommand( 'talkingmap_markersDialog' ));

		// Create the toolbar button that executes the above command.
		editor.ui.addButton( 'Timestamp', {
			label: 'Insert Marker Action',
			command: 'talkingmap_markers',
			toolbar: 'others',
      icon: this.path +'icons/talkingmap_markers.png'
		});
    
     CKEDITOR.dialog.add( 'talkingmap_markersDialog', this.path + 'dialogs/talkingmap_markers.js' );
	}
});